module gitlab.com/auk-go/extendcore

go 1.17

require (
	gitlab.com/auk-go/core v1.4.3
	gitlab.com/auk-go/enum v0.5.3
	gitlab.com/auk-go/errorwrapper v1.2.0
)

require golang.org/x/sys v0.12.0 // indirect
